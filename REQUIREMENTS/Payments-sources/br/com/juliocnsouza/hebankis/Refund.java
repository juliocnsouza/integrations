
package br.com.juliocnsouza.hebankis;


public class Refund {

    public String id;
    public Integer amount;
    public String currency;
    public String payment;
    public String status;
    public String createdDate;
    public String paidDate;
    public String canceledDate;

}
