
package br.com.juliocnsouza.hebankis;

import java.util.List;

public class Payment {

    public String id;
    public String createdAt;
    public String paidAt;
    public Boolean pinned;
    public String country;
    public List<Product> products = null;
    public Merchant merchant;
    public Integer taxes;
    public Double amountTotal;
    public String currency;
    public String productType;
    public String paymentType;
    public List<Refund> refunds = null;
    public String status;
    public PaymentLocation paymentLocation;
    public Tracking tracking;

}
